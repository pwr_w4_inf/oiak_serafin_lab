#include <stdio.h>
#include <stdlib.h>


typedef struct{
    float x0,x1,x2,x3;
}Vector;

float calcDet(Vector v1, Vector v2, Vector v3, Vector v4){
    Vector result;

    asm(
        //     1       |       2
// detM = (a0*b1-a1*b0)|*(c2*d3-c3*d2) +
        //(a2*b0-a0*b2)|*(c1*d3-c3*d1) +
        //(a0*b3-a3*b0)|*(c1*d2-c2*d1) +
        //(a1*b2-a2*b1)|*(c0*d3-c3*d0) +
        //-----3-------|-------4---------------
        //(a2*b3-a3*b2)|*(c0*d1-c1*d0) +
        //(a3*b1-a1*b3)|*(c0*d2-c2*d0)
        //             |

    //1
    "movups %1, %%xmm0 \n\t"
	"movups %2, %%xmm1 \n\t"
	"shufps $0x48, %%xmm0, %%xmm0 \n\t"
	"shufps $0xB1, %%xmm1, %%xmm1 \n\t"
    
    "mulps %%xmm1, %%xmm0 \n\t" //xmm0 zajete

    "movups %1, %%xmm1 \n\t"
	"movups %2, %%xmm2 \n\t"
	"shufps $0xB1, %%xmm1, %%xmm1 \n\t"
	"shufps $0x48, %%xmm2, %%xmm2 \n\t"

    "mulps %%xmm2, %%xmm1 \n\t" //xmm1 zajete

    "subps %%xmm0, %%xmm1 \n\t" //xmm0 i xmm1 wolne, xmm1 zajete - wynik 1 dzialania

    //2
    "movups %3, %%xmm0 \n\t"
	"movups %4, %%xmm2 \n\t"
	"shufps $0x16, %%xmm0, %%xmm0 \n\t"
	"shufps $0xEF, %%xmm2, %%xmm2 \n\t"

    "mulps %%xmm2, %%xmm0 \n\t" //xmm0 zajete

    "movups %3, %%xmm2 \n\t"
	"movups %4, %%xmm3 \n\t"
	"shufps $0xEF, %%xmm2, %%xmm2 \n\t"
	"shufps $0x16, %%xmm3, %%xmm3 \n\t"

    "mulps %%xmm3, %%xmm2 \n\t" //xmm2 zajete

    "subps %%xmm0, %%xmm2 \n\t" //xmm0 i xmm2 wolne, xmm2 zajete - wynik 2 dzialania

    //1*2
    "mulps %%xmm2, %%xmm1 \n\t" //xmm1 zajete, wynik 1 i 2 dzialania

    //3
    "movups %1, %%xmm0 \n\t"
	"movups %2, %%xmm2 \n\t"
	"shufps $0x0E, %%xmm0, %%xmm0 \n\t" //00 00 11 10
	"shufps $0x07, %%xmm2, %%xmm2 \n\t" //00 00 01 11

    "mulps %%xmm2, %%xmm0 \n\t" //xmm0 zajete

    "movups %1, %%xmm2 \n\t"
	"movups %2, %%xmm3 \n\t"
	"shufps $0x07, %%xmm2, %%xmm2 \n\t" //00 00 01 11
	"shufps $0x0E, %%xmm3, %%xmm3 \n\t" //00 00 11 10

    "mulps %%xmm3, %%xmm2 \n\t" //xmm2 zajete

    "subps %%xmm0, %%xmm2 \n\t" //xmm1 wolne, xmm2 zajete

    //4
    "movups %3, %%xmm0 \n\t"
	"movups %4, %%xmm3 \n\t"
	"shufps $0x00, %%xmm0, %%xmm0 \n\t"
	"shufps $0x09, %%xmm3, %%xmm3 \n\t"

    "mulps %%xmm3, %%xmm0 \n\t" //xmm0 zajete

    "movups %3, %%xmm3 \n\t"
	"movups %4, %%xmm4 \n\t"
	"shufps $0x09, %%xmm3, %%xmm3 \n\t"
	"shufps $0x00, %%xmm4, %%xmm4 \n\t"

    "mulps %%xmm4, %%xmm3 \n\t" //xmm3 zajete

    "subps %%xmm0, %%xmm3 \n\t" //xmm0 wolne, xmm3 zajete

    //3*4
    "mulps %%xmm3, %%xmm2 \n\t" //xmm2 zajete, wynik 1 i 2 dzialania
    
    //(1*2)+(3*4)
    "addps %%xmm2, %%xmm1 \n\t"

    "movups %%xmm1, %0 \n\t"

        :"=g" (result)
	    :"g"(v1),"g"(v2),"g"(v3),"g"(v4)
    );    

    return result.x0+result.x1+result.x2+result.x3;
}

int main(){
    
    float matrixDef[4][4] = {
        {1,2,8,4},
        {5,6,7,8},
        {1,10,11,22},
        {56,12,78,13}
    };

    Vector matrix[4];

    for(int i=0;i<4;i++){
        matrix[i].x0 = matrixDef[i][0];
        matrix[i].x1 = matrixDef[i][1];
        matrix[i].x2 = matrixDef[i][2];
        matrix[i].x3 = matrixDef[i][3];
    }

    for(int i=0;i<4;i++){
        printf("%f ",matrix[i].x0);
        printf("%f ",matrix[i].x1);
        printf("%f ",matrix[i].x2);
        printf("%f",matrix[i].x3);
        printf("\n");
    }



    printf("\n Result: %f\n", calcDet(matrix[0],matrix[1],matrix[2],matrix[3]));
    
    return 0;
}


